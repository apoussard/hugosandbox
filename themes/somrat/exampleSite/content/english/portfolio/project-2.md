---
title: "E-Marketing étude de la marque Tealer."
date: 2020-05-12T12:14:34+06:00
image: "images/portfolio/item2.jpg"
categories: ["design","development"]
description: "This is meta description."
draft: false
project_info:
- name: "Pour"
  icon: "fas fa-user"
  content: "Cours de E-marketing soutenance"
- name: "Project Link"
  icon: "fas fa-link"
  content: "https://localhost:9011/"
- name: "New Item"
  icon: "fas fa-globe"
  content: "Add whatever you want"
- name: "Loop Item"
  icon: "fas fa-redo"
  content: "This is in a loop"
---

Ce projet est le résultat de l'étude de nous avons mené pour améliorer le site web de la marque TEALER. Ce suuport est le support que nous avons présenté lors de notre soutenance dans le cadre du cours "E-marketing"


#### Détails du projet

La mission était claire, nous devions réaliser un audit du site internet dans le but de le corriger en proposant des recommandations. Nous avons également proposé une maquette sur wordpress. Des actions au niveau du référencement naturel ont été exposées (SEO) et nous avons également construit une structure Google Ads et une campagne de E-communication.


#### Compétences utilisées

Pour réaliser ce projet, nous avons mis en place une grille d'audit permettant d'établir un scoring de notre sujet vis à vis de ses concurrents. Des connaissances en référencement naturel et payant ont été mise en pratique et nous avons également proposer une campagne de E-communication sur les réseaux sociaux.