---
title: "Communication commerciale"
date: 2020-05-12T12:14:34+06:00
image: "images/portfolio/item3.jpg"
categories: ["database"]
description: "This is meta description."
draft: false
project_info:
- name: "Pour"
  icon: "fas fa-user"
  content: "IUT de Bordeaux cours de communication commerciale."
- name: "Project Link"
  icon: "fas fa-link"
  content: "https://localhost:9011/"
- name: "New Item"
  icon: "fas fa-globe"
  content: "Add whatever you want"
- name: "Loop Item"
  icon: "fas fa-redo"
  content: "This is in a loop"
---

Ce travail a été réalisé dans le cadre de l'étude de la communication commerciale.


#### Détails du projet

Le principe était d'analyser une campagne de communication commerciale. Quoi de mieux que d'étudier un phénomène d'ampleur sur le web avec l'une des plus grande campagne de communication dont tout le monde connaît le nom. "Raid shadow legends"


#### Compétences utilisées

Afin de mettre en pratique les compétences de communication commerciale, le but était d'analyser la stratégie mise en place par raid shadow legends afin d'atteindre leur objectif. La particularité de cette campagne est son ampleur.