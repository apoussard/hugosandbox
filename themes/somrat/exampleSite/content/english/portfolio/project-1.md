---
title: "Vidéo Toolimo"
image: "images/portfolio/item1.jpg"
categories: ["design","development"]
description: "This is meta description."
draft: false
project_info:
- name: "Pour"
  icon: "fas fa-user"
  content: "Entreprise Toolimo"
- name: "La vidéo"
  icon: "fas fa-link"
  content: "https://youtu.be/n4SWwiBVEPY"
- name: "New Item"
  icon: "fas fa-globe"
  content: "Add whatever you want"
- name: "Loop Item"
  icon: "fas fa-redo"
  content: "This is in a loop"
---

Vidéo réalisée dans le cadre de mon stage mémoire pour l'entreprise diag and go / Toolimo.


#### Détails

Cette vidéo a été réalisée dans le cadre d'une communication autour des fonctionnalités de l'outil proposé par l'entreprise. il y a un double objectif de formation et d'introduction à l'outil pour les nouveaux partenaires grâce à cette vidéo qui sera placée également sur les comptes des nouveaux arrivants.


#### Compétences utilisées

Ce projet nous a permis d'appliquer des compétences en audio visuel mais aussi des compétences de montage vidéo en se servant notamment du logiciel iMovie.