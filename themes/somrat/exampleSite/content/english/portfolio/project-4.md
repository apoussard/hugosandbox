---
title: "Blog AIO"
date: 2020-05-12T12:14:34+06:00
image: "images/portfolio/item4.jpg"
categories: ["database"]
description: "This is meta description."
draft: false
project_info:
- name: "Pour"
  icon: "fas fa-user"
  content: "AIO"
- name: "Lien"
  icon: "fas fa-link"
  content: "https://aio.eu/fr/actualites/"
- name: "New Item"
  icon: "fas fa-globe"
  content: "Add whatever you want"
- name: "Loop Item"
  icon: "fas fa-redo"
  content: "This is in a loop"
---

Voici le maitin du blog de l'entreprise AIO que j'ai pu réalisé en janvier 2020


#### Détails du projet

En plus de mes tâches de community manager, le blog était l'une de mes missions. La rédaction d'articles sur l'entreprise, ses produits et ses valeurs permettent de renseigner et de communiquer.

#### Compétences utilisées

Cette tâche m'a permis de mettre un premier pas sur Wordpress mais surtout dans le montage vidéo dont je me sers beaucoup depuis.