---
title: "Facebook Tech de co"
date: 2020-05-12T12:14:34+06:00
image: "images/portfolio/item5.jpg"
categories: ["design","sketch"]
description: "This is meta description."
draft: false
project_info:
- name: "Pour"
  icon: "fas fa-user"
  content: "DUT Techniques de commercialisation"
- name: "Lien"
  icon: "fas fa-link"
  content: "https://www.facebook.com/TCBordeaux"
- name: "New Item"
  icon: "fas fa-globe"
  content: "Add whatever you want"
- name: "Loop Item"
  icon: "fas fa-redo"
  content: "This is in a loop"
---

Cet onglet permet de montrer le travail de community management qui a été effectué par une équipe de 3 personnes dont je fais partie.


#### Détails du projet

Le principe était de réaliser tout au long de l'année une communication institutionnelle sur les différents évènements du DUT techniques de commercialisation afin de mettre en avant les compétences de la formation pour les partenaires et les futurs étudiants.


#### Compétences utilisées 

Différentes compétences ont pu être mises en application. Montage vidéo, écriture, logiciels type canva...