---
title : "PLUS D'INFORMATIONS ?"
bg_image : "images/backgrounds/need-service.jpg"
button:
  enable : true
  label : "CONTACTEZ MOI!"
  link : "#contact"


# custom style
custom_class: ""
custom_attributes: ""
custom_css: ""
---