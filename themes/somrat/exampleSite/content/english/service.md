---
title : "Informatique"
service_list:
# service item loop
- name : "Google Analytics"
  image : "images/icons/web-development.png"
  
# service item loop
- name : "Lumen5"
  image : "images/icons/graphic-design.png"
  
# service item loop
- name : "iMovie"
  image : "images/icons/dbms.png"
  
# service item loop
- name : "Pack Office"
  image : "images/icons/software-development.png"
  
# service item loop
- name : "Hubspot"
  image : "images/icons/marketing.png"
  
# service item loop
- name : "WordPress"
  image : "images/icons/mobile-app.png"



# custom style
custom_class: "" 
custom_attributes: "" 
custom_css: ""
---