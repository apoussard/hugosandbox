---
title : "APPRENEZ EN PLUS <br> SUR MOI"
image : "images/backgrounds/portrait.jpg"
# button
button:
  enable : true
  label : "Télécharger mon CV"
  link : "#"

########################### Experience ##############################
experience:
  enable : true
  title : "Expériences professionnelles"
  experience_list:
    # experience item loop
    - name : "Stage asistant administratif ventes et marketing"
      company : "Diag and go / Toolimo"
      duration : "Bordeaux / De Janvier 2021 à Mars 2021"
      content : "Gestion des commandes et des devis, prospection, suivi des partenaires. Création d'un process pour le poste, Réalisation de vidéos de formations et de communications."

    # experience item loop
    - name : "Projet Tuteuré, Community Manager"
      company : "IUT Bordeaux Bastide"
      duration : "Bordeaux / Depuis septembre 2021"
      content : "Gestion des réseaux sociaux, création de contenu"

    # experience item loop
    - name : "Saisonnier responsable accueil"
      company : "HPA La forêt"
      duration : "Saint-Jean-de-Monts / De juillet 2020 à septembre 2020"
      content : "Accueil, Relation clients internationaux, Factures, Fichier client, Relations commerciales locales, Entretien, Réservations."
    - name : "Stage Marketing numérique"
      company : "AIO"
      duration : "Pessac / De janvier 2020 à Février 2020"
      content : "Community management, E mailing, Organisation d'évènement, Rédacteur Blog, Veille concurrentielle, étude du trafic du site et optimisation (Google Analytics), aide au référencement."
    - name : "Employé polyvalent"
      company : "Biocoop"
      duration : "Cestas / De juillet 2019 à septembre 2019"
      content : "Vente, Conseil client, Gestion de stock, Gestion de rayon, Inventaire, Caisse."

############################### Skill #################################
skill:
  enable : true
  title : "Compétences"
  skill_list:
    # skill item loop
    - name : "Concevoir et mettre en oeuvre une opération de prospection"
      percentage : "98%"

    # skill item loop
    - name : "Compréhension et analyse d'une comptabilité"
      percentage : "90%"

    # skill item loop
    - name : "Stratégie marketing et budgétisation"
      percentage : "90%"

    # skill item loop
    - name : "Prise de décisions de gestions"
      percentage : "80%"
    - name : "Anglais"
      percentage : "75%"

formations:
  enable : true
  title : "Formations"
  experience_list:
    # experience item loop
    - name : "Licence Professionnelle E-commerce et Marketing Digital"
      company : "Université de Bordeaux"
      duration : "Bordeaux / De septembre 2021 à septembre 2022"
      content : "GStratégie E-Marketing (SEO, SEA, E-mailing...), Communication digitale (Webdesign, Community management), Langage Web (HTML, CSS, MySQL...), Utilisation de CMS et conception de site web, Expérience utilisateur, Analyse et mesure d'audience (google Analytics)."

    # experience item loop
    - name : "DUT Techniques de commercialisation"
      company : "IUT Bordeaux Bastide"
      duration : "Bordeaux / De septembre 2019 à septembre 2021 "
      content : "Marketing, Recherche commerciale, Communication, comptabilité, Droit , Négociations, E-Marketing."

    # experience item loop
    - name : "CPGEM (classe préparatoire aux grandes écoles militaires)"
      company : "Prytanée national militaire"
      duration : "La Flèche / De septembre 2018 à juillet 2019"
      content : "Voie économique et commerciale."
    - name : "Baccalauréat Economique et Social"
      company : "Lycée sans frontière"
      duration : "Pessac / De septembre 2015 à juillet 2018"
      content : "Mention Bien, Section européenne Anglais."
   



# custom style
custom_class: ""
custom_attributes: ""
custom_css: ""
---

Je suis Louis Poussard étudiant en deuxième année de DUT Techniques de commercialisation de l'IUT de Bordeaux. Motivé par mes précédentes expériences en Community Management et Web Analytics, je souhaite intégrer la licence professionnelle E-commerce et Marketing numérique pour la rentrée en Septembre 2021. <br> Animé par une curiosité des technologies du Web, je saurais mettre ma motivation et ma rigueur au service de l'entreprise .😊