---
title : "Je suis Louis Poussard"
# full screen navigation
first_name : "Louis"
last_name : "Poussard"
bg_image : "images/backgrounds/full-nav-bg.jpg"
# animated text loop
occupations:
- "Community manager"
- "Web Designer"
- "Trafic manager"

# slider background image loop
slider_images:
- "images/slider/slider-1.jpg"
- "images/slider/slider-2.jpg"
- "images/slider/slider-3.jpg"

# button
button:
  enable : true
  label : "Embauchez moi"
  link : "#contact"


# custom style
custom_class: ""
custom_attributes: ""
custom_css: ""

---